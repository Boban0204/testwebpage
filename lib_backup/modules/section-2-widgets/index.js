module.exports = {        
  extend: 'apostrophe-widgets',        
  label: 'Section 2',        
  contextual: true,
  addFields: [
    {
      name: 'oversattningImage',
      label: 'Oversattning Image',
      type: 'attachment',
      widgetType: 'apostrophe-images'
  },
  {
    name: 'tolkningImage',
    label: 'Tolkning Image',
    type: 'attachment',
    widgetType: 'apostrophe-images'
},
    {
      name: 'areaLeft',
      type: 'area',
      label: 'Left Area',
      options: {
        widgets: {
            'apostrophe-rich-text': {
              toolbar: [ 'Styles', 'Bold', 'Italic', 'Link', 'Unlink', 'Anchor', 'Table', 'BulletedList', 'Blockquote', 'Strike',  'Subscript', 'Superscript', 'Split' ],
              styles: [
              { name: 'H1', element: 'h1'},
              { name: 'H2', element: 'h2'},
              { name: 'H3', element: 'h3'},
              { name: 'H4', element: 'h4'},
              { name: 'H5', element: 'h5'},
              { name: 'H6', element: 'h6'},
              { name: 'Heading', element: 'h3' },
              { name: 'HeadingWhite', element: 'h3', styles: { 'color': '#fff' } },
              { name: 'HeadingWhite', element: 'h3', styles: { 'color': '#b6911f','text-align': 'center' } },
              { name: 'HeadingWhite', element: 'h2', styles: { 'color': '#b6911f','text-align': 'center' } },
              { name: 'HeadingWhite', element: 'h1', styles: { 'color': '#b6911f','text-align': 'center' } },
              { name: 'Subheading', element: 'h4' },
              { name: 'Paragraph', element: 'p' },
              { name: 'Centered Heading', element: 'h3', styles: { 'text-align': 'center'} }
              ] 
            }
        }
    }
    },
    {
      name: 'areaRight',
      type: 'area',
      label: 'Right Area',
      options: {
        widgets: {
        'apostrophe-rich-text': {
          toolbar: [ 'Styles', 'Bold', 'Italic', 'Link', 'Unlink', 'Anchor', 'Table', 'BulletedList', 'Blockquote', 'Strike',  'Subscript', 'Superscript', 'Split' ],
          styles: [
          { name: 'H1', element: 'h1'},
          { name: 'H2', element: 'h2'},
          { name: 'H3', element: 'h3'},
          { name: 'H4', element: 'h4'},
          { name: 'H5', element: 'h5'},
          { name: 'H6', element: 'h6'},
          { name: 'Heading', element: 'h3' },
          { name: 'HeadingWhite', element: 'h3', styles: { 'color': '#fff' } },
          { name: 'HeadingWhite', element: 'h3', styles: { 'color': '#b6911f','text-align': 'center' } },
          { name: 'HeadingWhite', element: 'h2', styles: { 'color': '#b6911f','text-align': 'center' } },
          { name: 'HeadingWhite', element: 'h1', styles: { 'color': '#b6911f','text-align': 'center' } },
          { name: 'Subheading', element: 'h4' },
          { name: 'Paragraph', element: 'p' },
          { name: 'Centered Heading', element: 'h3', styles: { 'text-align': 'center'} }
          ] 
      }
    }
    }
  }
  ]
};