module.exports = {        
  extend: 'apostrophe-widgets',        
  label: 'Section 1',        
  contextualOnly: true,
  addFields: [
    {
      name: 'areaLeft',
      type: 'area',
      label: 'Left Area',
    },
    {
      name: 'areaRight',
      type: 'area',
      label: 'Right Area',
    },
    {
      name: 'areaMenu',
      type: 'area',
      label: 'Menu Area',
    }
  ]
};