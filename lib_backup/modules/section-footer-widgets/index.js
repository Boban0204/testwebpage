module.exports = {        
  extend: 'apostrophe-widgets',        
  label: 'Section Footer',        
  contextualOnly: true,
  addFields: [
    {
      name: 'footerText1',
      type: 'area',
      label: 'footerText1',
    },
    {
      name: 'footerText2',
      type: 'area',
      label: 'footerText2',
    }
    ,
    {
      name: 'footerText3',
      type: 'area',
      label: 'footerText3',
    }
    ,
    {
      name: 'footerText4',
      type: 'area',
      label: 'footerText4',
    }
  ]
};