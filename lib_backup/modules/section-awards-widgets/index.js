module.exports = {        
  extend: 'apostrophe-widgets',        
  label: 'Section Awards',        
  contextualOnly: true,
  addFields: [
    {
      name: 'sectionTitle',
      type: 'area',
      label: 'Section Title',
    },
    {
      name: 'awardImage1',
      type: 'area',
      label: 'Award Image1',
    },
    {
      name: 'awardImage2',
      type: 'area',
      label: 'Award Image2',
    },
    {
      name: 'awardImage3',
      type: 'area',
      label: 'Award Image3',
    },
    {
      name: 'awardImage4',
      type: 'area',
      label: 'Award Image4',
    },
    {
      name: 'awardImage5',
      type: 'area',
      label: 'Award Image5',
    }
  ]
};