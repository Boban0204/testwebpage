module.exports = {        
  extend: 'apostrophe-widgets',        
  label: 'Section Tabs Collapse',        
  contextualOnly: true,
  addFields: [
    {
      name: 'areaUp',
      type: 'area',
      label: 'Up Area',
    },
    {
      name: 'areaDown',
      type: 'area',
      label: 'Down Area',
    }
  ]
};