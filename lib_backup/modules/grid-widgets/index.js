module.exports = {        
  extend: 'apostrophe-widgets',        
  label: 'Grid',        
  addFields: [
    {
      name: 'grid',
      type: 'select',
      choices:[
        {label:'10%',value:'col-md-2'}
        ,{label:'25%',value:'col-md-4'}
        ,{label:'30%',value:'col-md-3'}
        ,{label:'50%',value:'col-md-6'}
        ,{label:'70%',value:'col-md-8'}
        ,{label:'100%',value:'col-md-12'}
      ]
    },
    {
      name: 'sectionHeading',
      label: 'Section Heading',
      type: 'area'
    }
  ]
};