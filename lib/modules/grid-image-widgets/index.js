module.exports = {        
  extend: 'apostrophe-widgets',        
  label: 'Grid Image',        
  addFields: [
    {
      name: 'grid',
      type: 'select',
      choices:[
        {label:'10%',value:'col-md-2'}
        ,{label:'30%',value:'col-md-3'}
        ,{label:'50%',value:'col-md-6'}
        ,{label:'70%',value:'col-md-8'}
        ,{label:'100%',value:'col-md-12'}
      ]
    },
    {
      name: 'areaLeft',
      type: 'area',
      label: 'Left Area',
    }
  ]        
};