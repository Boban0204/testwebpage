module.exports = {        
  extend: 'apostrophe-widgets',        
  label: 'Form Register',        
  addFields: [
    {
      name: 'sectionMainTitle',
      type: 'area',
      label: 'Section Title',
    },
    {
      name: 'sectionTitle',
      type: 'area',
      label: 'Section Title',
    },
    {
      name: 'sectionSubtitle1',
      type: 'area',
      label: 'Section Title',
    },
    {
      name: 'sectionSubtitle2',
      type: 'area',
      label: 'Section Title',
    }
  ]        
};