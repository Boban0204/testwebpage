module.exports = {        
  extend: 'apostrophe-widgets',
  label: 'Section 3',
  addFields: [
    {
      name: 'areaLeft',
      type: 'area',
      label: 'Left Area',
      options: {
        widgets: {
            'apostrophe-rich-text': {
              toolbar: [ 'Styles', 'Bold', 'Italic', 'Link', 'Unlink', 'Anchor', 'Table', 'BulletedList', 'Blockquote', 'Strike',  'Subscript', 'Superscript', 'Split' ],
              styles: [
                { name: 'H1', element: 'h1', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H1 C', element: 'h1', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H1 W', element: 'h1', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H1 W C', element: 'h1', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H1 S', element: 'h1', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H1 S C', element: 'h1', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2', element: 'h2', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H2 C', element: 'h2', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H2 W', element: 'h2', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2 W C', element: 'h2', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2 S', element: 'h2', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2 S C', element: 'h2', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3', element: 'h3', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H3 C', element: 'h3', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H3 W', element: 'h3', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3 W C', element: 'h3', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3 S', element: 'h3', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3 S C', element: 'h3', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4', element: 'h4', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H4 C', element: 'h4', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H4 W', element: 'h4', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4 W C', element: 'h4', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4 S', element: 'h4', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4 S C', element: 'h4', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5', element: 'h5', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H5 C', element: 'h5', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H5 W', element: 'h5', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5 W C', element: 'h5', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5 S', element: 'h5', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5 S C', element: 'h5', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H6', element: 'h6', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H6 C', element: 'h6', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H6 W', element: 'h6', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H6 W C', element: 'h6', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H6 S', element: 'h6', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H6 S C', element: 'h6', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'Normal', element: 'p', styles: { 'line-height': '1.56', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'Normal B', element: 'p', styles: { 'font-weight': 'bold', 'line-height': '1.56', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'Normal B C', element: 'p', styles: { 'font-weight': 'bold', 'text-align': 'center', 'line-height': '1.56', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'Normal W', element: 'p', styles: { 'color': '#fff', 'line-height': '1.56', 'letter-spacing': 'normal' } },
                { name: 'Normal W B', element: 'p', styles: { 'color': '#fff', 'font-weight': 'bold', 'line-height': '1.56', 'letter-spacing': 'normal' } },
                { name: 'Normal W B C', element: 'p', styles: { 'color': '#fff', 'font-weight': 'bold', 'text-align': 'center', 'line-height': '1.56', 'letter-spacing': 'normal' } },
                { name: 'Normal S', element: 'p', styles: { 'color': '#b6911f', 'line-height': '1.56', 'letter-spacing': 'normal' } },
                { name: 'Normal S B', element: 'p', styles: { 'color': '#b6911f', 'font-weight': 'bold', 'line-height': '1.56', 'letter-spacing': 'normal' } },
                { name: 'Normal S B C', element: 'p', styles: { 'color': '#b6911f', 'font-weight': 'bold', 'text-align': 'center', 'line-height': '1.56', 'letter-spacing': 'normal' } }
                ]
            },
            'apostrophe-images': {
              size: 'one-third'
          }
        }
    }
    },
    {
      name: 'areaRight',
      type: 'area',
      label: 'Right Area',
      options: {
        widgets: {
            'apostrophe-rich-text': {
              toolbar: [ 'Styles', 'Bold', 'Italic', 'Link', 'Unlink', 'Anchor', 'Table', 'BulletedList', 'Blockquote', 'Strike',  'Subscript', 'Superscript', 'Split' ],
              styles: [
                { name: 'H1', element: 'h1', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H1 C', element: 'h1', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H1 W', element: 'h1', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H1 W C', element: 'h1', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H1 S', element: 'h1', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H1 S C', element: 'h1', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2', element: 'h2', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H2 C', element: 'h2', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H2 W', element: 'h2', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2 W C', element: 'h2', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2 S', element: 'h2', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2 S C', element: 'h2', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3', element: 'h3', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H3 C', element: 'h3', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H3 W', element: 'h3', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3 W C', element: 'h3', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3 S', element: 'h3', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3 S C', element: 'h3', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4', element: 'h4', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H4 C', element: 'h4', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H4 W', element: 'h4', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4 W C', element: 'h4', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4 S', element: 'h4', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4 S C', element: 'h4', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5', element: 'h5', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H5 C', element: 'h5', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H5 W', element: 'h5', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5 W C', element: 'h5', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5 S', element: 'h5', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5 S C', element: 'h5', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H6', element: 'h6', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H6 C', element: 'h6', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H6 W', element: 'h6', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H6 W C', element: 'h6', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H6 S', element: 'h6', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H6 S C', element: 'h6', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'Normal', element: 'p', styles: { 'line-height': '1.56', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'Normal B', element: 'p', styles: { 'font-weight': 'bold', 'line-height': '1.56', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'Normal B C', element: 'p', styles: { 'font-weight': 'bold', 'text-align': 'center', 'line-height': '1.56', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'Normal W', element: 'p', styles: { 'color': '#fff', 'line-height': '1.56', 'letter-spacing': 'normal' } },
                { name: 'Normal W B', element: 'p', styles: { 'color': '#fff', 'font-weight': 'bold', 'line-height': '1.56', 'letter-spacing': 'normal' } },
                { name: 'Normal W B C', element: 'p', styles: { 'color': '#fff', 'font-weight': 'bold', 'text-align': 'center', 'line-height': '1.56', 'letter-spacing': 'normal' } },
                { name: 'Normal S', element: 'p', styles: { 'color': '#b6911f', 'line-height': '1.56', 'letter-spacing': 'normal' } },
                { name: 'Normal S B', element: 'p', styles: { 'color': '#b6911f', 'font-weight': 'bold', 'line-height': '1.56', 'letter-spacing': 'normal' } },
                { name: 'Normal S B C', element: 'p', styles: { 'color': '#b6911f', 'font-weight': 'bold', 'text-align': 'center', 'line-height': '1.56', 'letter-spacing': 'normal' } }
                ]
            }, 'apostrophe-images': {
              size: 'one-third'
          }
        }
    }
    },
    {
      name: 'buttonOne',
      type: 'area',
      label: 'Button One Text',
      options: {
        widgets: {
            'apostrophe-rich-text': {
              toolbar: [ 'Styles', 'Bold', 'Italic', 'Link', 'Unlink', 'Anchor', 'Table', 'BulletedList', 'Blockquote', 'Strike',  'Subscript', 'Superscript', 'Split' ],
              styles: [
                { name: 'Normal', element: 'p', styles: { 'line-height': '1.56', 'letter-spacing': 'normal' } },
                { name: 'Normal B', element: 'p', styles: { 'font-weight': 'bold', 'line-height': '1.56', 'letter-spacing': 'normal' } }
                ]
            }
        }
    }
    },
    {
      name: 'buttonTwo',
      type: 'area',
      label: 'Button Two Text',
      options: {
        widgets: {
            'apostrophe-rich-text': {
              toolbar: [ 'Styles', 'Bold', 'Italic', 'Link', 'Unlink', 'Anchor', 'Table', 'BulletedList', 'Blockquote', 'Strike',  'Subscript', 'Superscript', 'Split' ],
              styles: [
                { name: 'Normal', element: 'p', styles: { 'line-height': '1.56', 'letter-spacing': 'normal' } },
                { name: 'Normal B', element: 'p', styles: { 'font-weight': 'bold', 'line-height': '1.56', 'letter-spacing': 'normal' } }
                ]
            }
        }
    }
    }
  ]
};  