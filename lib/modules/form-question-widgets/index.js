module.exports = {        
  extend: 'apostrophe-widgets',
  label: 'Form Question',
  contextualOnly: true,
  addFields: [
    {
      name: 'sectionTitle',
      type: 'area',
      label: 'Section Title'
    }
  ]        
};