var _ = require('lodash')
  , async = require('async')
  , moment = require('moment');

module.exports = {
  name: 'apostrophe-blog',
  alias: 'blog',
  label: 'Article',
  extend: 'apostrophe-pieces',
  contextual: true,

  moogBundle: {
    modules: ['apostrophe-blog-pages', 'apostrophe-blog-widgets'],
    directory: 'lib/modules'
  },
  
  addFields: [
    {
      name: 'shortDescription',
      label: 'Short Description',
      type: 'string',
      textarea: true,
      max: 240
    },
    {
      name: 'thumbnail',
      label: 'Upload Image: 1500x500 32dpi',
      type: 'attachment',
      widgetType: 'apostrophe-images'
  }
  ],
  piecesFilters: [
    {
      name: 'year'
    }
  ],

  beforeConstruct: function(self, options) {

    options.sort = { publishedAt: -1 };

    options.addFields = [
      {
        name: 'publishedAt',
        label: 'Publication Date',
        type: 'date',
        required: true,
        sortable: true
      },
    //   {
    //   name: 'body',
    //   label: 'Body',
    //   type: 'area',
    //   options: {
    //     widgets: {
    //       'apostrophe-rich-text': {
    //         toolbar: [ 'Bold', 'Italic', 'Link', 'Unlink' ]
    //       },
    //       'apostrophe-images': {}
    //     }
    //   }
    // },
    // {
    //   name: 'thumbnail',
    //   label: 'Thumbnail',
    //   type: 'singleton',
    //   widgetType: 'apostrophe-images',
    //   options: {
    //     limit: 1,
    //     minSize: [ 200, 200 ],
    //     aspectRatio: [ 1, 1 ]
    //   }
    // }
    ].concat(options.addFields || []);

    options.arrangeFields = _.merge([
      { name: 'basic', label: 'Basics', fields: ['title', 'slug', 'shortDescription', 'thumbnail', 'publishedAt'] },
      { name: 'meta', label: 'Meta', fields: ['tags','published'] },
    ], options.arrangeFields || []);

    options.addColumns = [
      {
        name: 'publishedAt',
        label: 'Publication Date',
      }
    ].concat(options.addColumns || []);

    options.addSorts = [
      {
        name: 'publishedAt',
        label: 'By Publication Date',
        sort: { startDate: -1 }
      }
    ].concat(options.addSorts || []);

    options.addFilters = [
      {
        name: 'future',
        choices: [
          {
            value: true,
            label: 'Future'
          },
          {
            value: false,
            label: 'Past'
          },
          {
            value: null,
            label: 'Both'
          }
        ],
        def: null
      }
    ].concat(options.addFilters || []);
  },

  construct: function(self, options) {

    // limit the results of autocomplete for joins
    // so they only include past posts
    self.extendAutocompleteCursor = function(cursor) {
      return cursor.future(false);
    };

    // When editing we don't care if the blog post is in the future
    var superFindForEditing = self.findForEditing;
    self.findForEditing = function(req, criteria, projection) {
      return superFindForEditing(req, criteria, projection).future(null);
    };

    var superNewInstance = self.newInstance;
    self.newInstance = function() {
      var instance = superNewInstance();
      // Correct handling of dynamic default. If you do this in the schema
      // you wind up with the day the server launched
      if (!instance.publishedAt) {
        var now = moment();
        instance.publishedAt = now.format('YYYY-MM-DD');
      }
      return instance;
    };
  }
};