module.exports = {        
  extend: 'apostrophe-widgets',        
  label: 'Layout 2/5 - 5/2',        
  addFields: [
    {
      name: 'areaLeft',
      type: 'area',
      label: 'Left Area',
      options: {
        widgets: {
            'apostrophe-images': {
              size: 'one-sixth'
            },
            'apostrophe-rich-text': {
              toolbar: [ 'Styles', 'Bold', 'Italic', 'Link', 'Unlink', 'Anchor', 'Table', 'BulletedList', 'Blockquote', 'Strike',  'Subscript', 'Superscript', 'Split' ],
              styles: [
                { name: 'H1', element: 'h1', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H1 C', element: 'h1', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H1 W', element: 'h1', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H1 W C', element: 'h1', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H1 S', element: 'h1', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H1 S C', element: 'h1', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2', element: 'h2', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H2 C', element: 'h2', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H2 W', element: 'h2', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2 W C', element: 'h2', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2 S', element: 'h2', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2 S C', element: 'h2', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3', element: 'h3', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H3 C', element: 'h3', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H3 W', element: 'h3', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3 W C', element: 'h3', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3 S', element: 'h3', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3 S C', element: 'h3', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4', element: 'h4', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H4 C', element: 'h4', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H4 W', element: 'h4', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4 W C', element: 'h4', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4 S', element: 'h4', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4 S C', element: 'h4', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5', element: 'h5', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H5 C', element: 'h5', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H5 W', element: 'h5', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5 W C', element: 'h5', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5 S', element: 'h5', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5 S C', element: 'h5', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'Hero', element: 'p', styles: { 'font-size': '20px', 'font-family': 'gotham, sans-serif', 'color': '#323232' } },
                { name: 'Hero B', element: 'p', styles: { 'font-weight': 'bold', 'font-size': '20px', 'font-family': 'gotham, sans-serif', 'color': '#323232' } },
                { name: 'Hero B C', element: 'p', styles: { 'font-weight': 'bold', 'text-align': 'center', 'font-size': '20px', 'font-family': 'gotham, sans-serif', 'color': '#323232' } },
                { name: 'Hero W', element: 'p', styles: { 'color': '#fff', 'font-size': '20px', 'font-family': 'gotham, sans-serif' } },
                { name: 'Hero W B', element: 'p', styles: { 'color': '#fff', 'font-weight': 'bold', 'font-size': '20px', 'font-family': 'gotham, sans-serif' } },
                { name: 'Hero W B C', element: 'p', styles: { 'color': '#fff', 'font-weight': 'bold', 'text-align': 'center', 'font-size': '20px', 'font-family': 'gotham, sans-serif' } },
                { name: 'Hero S', element: 'p', styles: { 'color': '#b6911f', 'font-size': '20px', 'font-family': 'gotham, sans-serif' } },
                { name: 'Hero S B', element: 'p', styles: { 'color': '#b6911f', 'font-weight': 'bold', 'font-size': '20px', 'font-family': 'gotham, sans-serif' } },
                { name: 'Hero S B C', element: 'p', styles: { 'color': '#b6911f', 'font-weight': 'bold', 'text-align': 'center', 'font-size': '20px', 'font-family': 'gotham, sans-serif' } },
                { name: 'HeroBig', element: 'p', styles: { 'font-size': '24px', 'font-family': 'gotham, sans-serif', 'color': '#323232' } },
                { name: 'HeroBig B', element: 'p', styles: { 'font-weight': 'bold', 'font-size': '24px', 'font-family': 'gotham, sans-serif', 'color': '#323232' } },
                { name: 'HeroBig B C', element: 'p', styles: { 'font-weight': 'bold', 'text-align': 'center', 'font-size': '24px', 'font-family': 'gotham, sans-serif', 'color': '#323232' } },
                { name: 'HeroBig W', element: 'p', styles: { 'color': '#fff', 'font-size': '24px', 'font-family': 'gotham, sans-serif' } },
                { name: 'HeroBig W B', element: 'p', styles: { 'color': '#fff', 'font-weight': 'bold', 'font-size': '24px', 'font-family': 'gotham, sans-serif' } },
                { name: 'HeroBig W B C', element: 'p', styles: { 'color': '#fff', 'font-weight': 'bold', 'text-align': 'center', 'font-size': '24px', 'font-family': 'gotham, sans-serif' } },
                { name: 'HeroBig S', element: 'p', styles: { 'color': '#b6911f', 'font-size': '24px', 'font-family': 'gotham, sans-serif' } },
                { name: 'HeroBig S B', element: 'p', styles: { 'color': '#b6911f', 'font-weight': 'bold', 'font-size': '24px', 'font-family': 'gotham, sans-serif' } },
                { name: 'HeroBig S B C', element: 'p', styles: { 'color': '#b6911f', 'font-weight': 'bold', 'text-align': 'center', 'font-size': '24px', 'font-family': 'gotham, sans-serif' } }
                ]
            }
          }
        }
    },
    {
      name: 'areaRight',
      type: 'area',
      label: 'Right Area',
      options: {
        widgets: {
          'apostrophe-images': {
            size: 'one-half'
          },
            'apostrophe-rich-text': {
              toolbar: [ 'Styles', 'Bold', 'Italic', 'Link', 'Unlink', 'Anchor', 'Table', 'BulletedList', 'Blockquote', 'Strike',  'Subscript', 'Superscript', 'Split' ],
              styles: [
                { name: 'H1', element: 'h1', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H1 C', element: 'h1', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H1 W', element: 'h1', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H1 W C', element: 'h1', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H1 S', element: 'h1', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H1 S C', element: 'h1', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2', element: 'h2', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H2 C', element: 'h2', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H2 W', element: 'h2', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2 W C', element: 'h2', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2 S', element: 'h2', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H2 S C', element: 'h2', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3', element: 'h3', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H3 C', element: 'h3', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H3 W', element: 'h3', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3 W C', element: 'h3', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3 S', element: 'h3', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H3 S C', element: 'h3', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4', element: 'h4', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H4 C', element: 'h4', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H4 W', element: 'h4', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4 W C', element: 'h4', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4 S', element: 'h4', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H4 S C', element: 'h4', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5', element: 'h5', styles: { 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H5 C', element: 'h5', styles: { 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal', 'color': '#323232' } },
                { name: 'H5 W', element: 'h5', styles: { 'color': '#fff', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5 W C', element: 'h5', styles: { 'color': '#fff', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5 S', element: 'h5', styles: { 'color': '#b6911f', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'H5 S C', element: 'h5', styles: { 'color': '#b6911f', 'text-align': 'center', 'line-height': 'normal', 'letter-spacing': 'normal' } },
                { name: 'Hero', element: 'p', styles: { 'font-size': '20px', 'font-family': 'gotham, sans-serif', 'color': '#323232' } },
                { name: 'Hero B', element: 'p', styles: { 'font-weight': 'bold', 'font-size': '20px', 'font-family': 'gotham, sans-serif', 'color': '#323232' } },
                { name: 'Hero B C', element: 'p', styles: { 'font-weight': 'bold', 'text-align': 'center', 'font-size': '20px', 'font-family': 'gotham, sans-serif', 'color': '#323232' } },
                { name: 'Hero W', element: 'p', styles: { 'color': '#fff', 'font-size': '20px', 'font-family': 'gotham, sans-serif' } },
                { name: 'Hero W B', element: 'p', styles: { 'color': '#fff', 'font-weight': 'bold', 'font-size': '20px', 'font-family': 'gotham, sans-serif' } },
                { name: 'Hero W B C', element: 'p', styles: { 'color': '#fff', 'font-weight': 'bold', 'text-align': 'center', 'font-size': '20px', 'font-family': 'gotham, sans-serif' } },
                { name: 'Hero S', element: 'p', styles: { 'color': '#b6911f', 'font-size': '20px', 'font-family': 'gotham, sans-serif' } },
                { name: 'Hero S B', element: 'p', styles: { 'color': '#b6911f', 'font-weight': 'bold', 'font-size': '20px', 'font-family': 'gotham, sans-serif' } },
                { name: 'Hero S B C', element: 'p', styles: { 'color': '#b6911f', 'font-weight': 'bold', 'text-align': 'center', 'font-size': '20px', 'font-family': 'gotham, sans-serif' } },
                { name: 'HeroBig', element: 'p', styles: { 'font-size': '24px', 'font-family': 'gotham, sans-serif', 'color': '#323232' } },
                { name: 'HeroBig B', element: 'p', styles: { 'font-weight': 'bold', 'font-size': '24px', 'font-family': 'gotham, sans-serif', 'color': '#323232' } },
                { name: 'HeroBig B C', element: 'p', styles: { 'font-weight': 'bold', 'text-align': 'center', 'font-size': '24px', 'font-family': 'gotham, sans-serif', 'color': '#323232' } },
                { name: 'HeroBig W', element: 'p', styles: { 'color': '#fff', 'font-size': '24px', 'font-family': 'gotham, sans-serif' } },
                { name: 'HeroBig W B', element: 'p', styles: { 'color': '#fff', 'font-weight': 'bold', 'font-size': '24px', 'font-family': 'gotham, sans-serif' } },
                { name: 'HeroBig W B C', element: 'p', styles: { 'color': '#fff', 'font-weight': 'bold', 'text-align': 'center', 'font-size': '24px', 'font-family': 'gotham, sans-serif' } },
                { name: 'HeroBig S', element: 'p', styles: { 'color': '#b6911f', 'font-size': '24px', 'font-family': 'gotham, sans-serif' } },
                { name: 'HeroBig S B', element: 'p', styles: { 'color': '#b6911f', 'font-weight': 'bold', 'font-size': '24px', 'font-family': 'gotham, sans-serif' } },
                { name: 'HeroBig S B C', element: 'p', styles: { 'color': '#b6911f', 'font-weight': 'bold', 'text-align': 'center', 'font-size': '24px', 'font-family': 'gotham, sans-serif' } }
                ]
            }
          }
        }
    }
    ]        
  };