module.exports = {
  extend: 'apostrophe-widgets',
  label: 'Layout 1/1 Opacity',
  addFields: [
    {
      name: 'tolkningImage',
      label: 'Image',
      type: 'attachment',
      widgetType: 'apostrophe-images'
  },
  ]      
};