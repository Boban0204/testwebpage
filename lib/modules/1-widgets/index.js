module.exports = {        
  extend: 'apostrophe-widgets',        
  label: 'Layout 1/1',       
  contextualOnly: true, 
  addFields: [
    { 
      name: 'areaOne',
      type: 'area',
      label: 'Area One'
    }
  ]      
};