module.exports = {        
  extend: 'apostrophe-widgets',        
  label: 'Form Quotation',        
  addFields: [
    {
      name: 'sectionTitle',
      type: 'area',
      label: 'Section Title'
    },
    {
      name: 'sectionSubtitle',
      type: 'area',
      label: 'Section Subtitle'
    }
  ]        
};