module.exports = {
  name: 'apostrophe-blog-page',
  label: 'Blog Page',
  extend: 'apostrophe-pieces-pages',
  contextual: true,
  addFields: [
    {
      name: 'shortDescription',
      label: 'Short Description',
      type: 'string',
      textarea: true,
      max: 240
    },
    {
      name: 'thumbnail',
      label: 'Upload Image: 1500x500 32dpi',
      type: 'attachment',
      widgetType: 'apostrophe-images'
  }
],
piecesFilters: [
  {
    name: 'year'
  }
],

  construct: function(self, options) {
    // Make sure future filter is set to false
    var superIndexCursor = self.indexCursor;
    self.indexCursor = function(req) {
      return superIndexCursor(req).future(false);
    };
  }
};