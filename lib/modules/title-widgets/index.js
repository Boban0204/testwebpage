module.exports = {        
  extend: 'apostrophe-widgets',        
  label: 'Title',    
  addFields: [
    {
      name: 'title',
      type: 'area',
      label: 'Enter Page Title',
      options: {
        widgets: {
            'apostrophe-rich-text': {
              styles: [
              { name: 'H1', element: 'h1'},
              ] 
            }
        }
    }
    }
  ]        
};