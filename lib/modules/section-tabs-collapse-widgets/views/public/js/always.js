
        $(document).ready(function(){  
            // Add minus icon for collapse element which is open by default
            $(".collapse.in").each(function(){
                $(this).siblings(".panel-heading").find(".icon").addClass("chevron-down-circle-up").removeClass("chevron-down-circle");
            });

            // Toggle plus minus icon on show hide of collapse element
            $(".collapse").on('show.bs.collapse', function(){
                $(this).parent().find(".icon").removeClass("chevron-down-circle").addClass("chevron-down-circle-up");
            }).on('hide.bs.collapse', function(){
                $(this).parent().find(".icon").removeClass("chevron-down-circle-up").addClass("chevron-down-circle");
            });
        });