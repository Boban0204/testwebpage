module.exports = {        
  extend: 'apostrophe-widgets',
  label: 'Form Quality',
  contextualOnly: true,
  addFields: [
    {
      name: 'sectionTitle',
      type: 'area',
      label: 'Section Title'
    },
    {
      name: 'sectionSubtitle',
      type: 'area',
      label: 'Section Subtitle'
    }
  ]        
};