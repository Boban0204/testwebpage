var path = require('path');

var sanitizeHtml = require('sanitize-html');

var dirty = 'some really tacky HTML';
var clean = sanitizeHtml(dirty);

var apos = require('apostrophe')({
  shortName: 'Apostrophe',

  // See lib/modules for basic project-level configuration of our modules
  // responsible for serving static assets, managing page templates and
  // configuring user acounts.
  modules: {
  
  'apostrophe-search': {},
  'apostrophe-blog': {
    contextual: true
  },
  'apostrophe-blog-pages': {},
  'apostrophe-blog-widgets': {},
  'apostrophe-forms': {
      email: {
      from: "Boban Lazarov <bl@accent-skopje.mk>"
      }
    },
    // Apostrophe module configuration

    // Note: most configuration occurs in the respective
    // modules' directories. See lib/apostrophe-assets/index.js for an example.
    
    // However any modules that are not present by default in Apostrophe must at
    // least have a minimal configuration here: `moduleName: {}`

    // If a template is not found somewhere else, serve it from the top-level
    // `views/` folder of the project
    'apostrophe-workflow': {
      prefixes: {
        // Even private locales must be distinguishable by hostname and/or prefix
        'en-gb': '/en',
        'se': '/se'
        // We don't need prefixes for fr because
        // that hostname is not shared with other
        // locales
      },
      locales: [
        {
          name: 'default',
          label: 'Default',
          private: true,
          children: [
            {
              name: 'en-gb',
              label: 'England'
            },
            {
              name: 'se',
              label: 'Sweden'
            }
          ]
        },
      ],
      defaultLocale: 'default',
      // IMPORTANT: if you follow the examples below,
      // be sure to set this
      alias: 'workflow'
    },
    'apostrophe-templates': { viewsFolderFallback: path.join(__dirname, 'views') },
    'two-column-widgets': {},
    'section-1-widgets': {},
    'section-2-widgets': {},
    'section-3-widgets': {},
    'section-4-widgets': {},
    'section-5-widgets': {},
    'section-awards-widgets': {},
    'section-footer-widgets': {},
    'section-hero-widgets': {},
    'grid-widgets': {},
    'submenu-widgets': {},
    'info-message-widgets': {},
    'grid-image-widgets': {},
    'section-contact-divisions-widgets': {},
    'section-tabs-collapse-widgets': {},
    'section-tabs-collapse-three-widgets': {},
    'form-register-widgets': {},
    'form-login-request-widgets': {},
    'form-quality-widgets': {},
    'form-question-widgets': {},
    'form-quotation-widgets': {},
    'preview-forms-data-widgets': {},
    'title-widgets': {},
    '1-widgets': {},
    '1-opacity-widgets': {},
    'layout-23-13-widgets': {},
    'layout-13-23-widgets': {},
    'layout-2-5-5-2-widgets': {},
    'layout-5-2-2-5-widgets': {},
    'layout-1-2-widgets': {},
    'apostrophe-express': {
      csrf: {
        exceptions: [ '/my-post-route-url' ]
      }
    },
    'contact-form': {
      email: {
        from: "bl@accent-skopje.mk"
      }
    },
    'contact-form-widgets': {},
    // This configures our default page template
    'apostrophe-pages': {
      filters: {
        // Grab our ancestor pages, with two levels of subpages
        ancestors: {
          children: {
            depth: 2
          }
        },
        // We usually want children of the current page, too
        children: true
      },
      types: [
        {
          name: 'default',
          label: 'Default'
        },
        {
          name: 'home',
          label: 'Home'
        },
        {
          name: 'apostrophe-blog-page',
          label: 'Blog'
        }
      ],
      park: [
        {
          title: 'Search',
          slug: '/search',
          type: 'apostrophe-search',
          label: 'Search',
          published: true
        }
      ]
    }
  }
});